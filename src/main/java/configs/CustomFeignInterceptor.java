package configs;

import adapters.TokenAdapter;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class CustomFeignInterceptor implements RequestInterceptor {

    private final Map<String, String> traceHeaders;

    private final TokenAdapter tokenAdapter;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        traceHeaders
                .forEach(requestTemplate::header);

        String token = tokenAdapter.getToken();
        log.info("M2M token: {}", token);
        requestTemplate.header("Authorization", "Bearer " + token);

        String advisorCode = tokenAdapter.getAdvisorCode();
        requestTemplate.header("jwtClaims", advisorCode);

        requestTemplate.headers()
                .forEach((key, value) ->
                        log.info("Custom Required headers: key = {}, value = {}", key, value)
                );

    }
}
