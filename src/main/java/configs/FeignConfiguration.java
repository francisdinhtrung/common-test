package configs;

import adapters.LeadAdapter;
import adapters.NewBusinessAdapter;
import adapters.TokenAdapter;
import feign.Feign;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Configuration
@Slf4j
public class FeignConfiguration {

    private final String X_TRACEABILITY_ID = "id-traceability-id";
    private final String X_CORRELATION_ID = "id-correlation-id";

    @Bean
    public LeadAdapter leadAdapter() {
        return buildFeignClient(LeadAdapter.class, "lead-url");
    }

    @Bean
    public NewBusinessAdapter newBusinessAdapter() {
        return buildFeignClient(NewBusinessAdapter.class, "nb-url");
    }

    public TokenAdapter getTokenAdapter() {
        return new TokenAdapter();
    }

    private <T> T buildFeignClient(Class<T> clazz, String url) {
        return Feign.builder()
                .requestInterceptor(new CustomFeignInterceptor(getTraceIdHeader(), getTokenAdapter()))
                .target(clazz, url);
    }

    private Map<String, String> getTraceIdHeader() {
        Map<String, String> headers = new HashMap<>();
        String traceabilityId;
        String correlationId;
        try {
            // get header from incoming request
            HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))
                    .getRequest();

            traceabilityId = request.getHeader(X_TRACEABILITY_ID) != null
                    ? request.getHeader(X_TRACEABILITY_ID)
                    : UUID.randomUUID().toString();
            correlationId = request.getHeader(X_CORRELATION_ID) != null
                    ? request.getHeader(X_CORRELATION_ID)
                    : UUID.randomUUID().toString();

        } catch (Exception e) {
            log.warn("TraceId header get error: {}", e.getMessage());
            traceabilityId = UUID.randomUUID().toString();
            correlationId = UUID.randomUUID().toString();
        }
        headers.put(X_TRACEABILITY_ID, traceabilityId);
        headers.put(X_CORRELATION_ID, correlationId);
        return headers;
    }
}
